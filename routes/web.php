<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HelloController;
use App\Http\Controllers\PostController;
use App\Http\Middleware\TestMiddleware;
use App\Http\Controllers\LoginController;
Route::get('/login', [LoginController::class, 'login']);
Route::post('/auth', [LoginController::class, 'auth']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::middleware([TestMiddleware::class])->group(function() {
    Route::get('/post/create', [PostController::class, 'create']);
    Route::post('/post/store', [PostController::class, 'store'])->name('post.store');
    Route::any('/post/list', [PostController::class, 'list']);
    Route::get('/post/edit/{id}', [PostController::class, 'edit']);
    Route::post('/post/update', [PostController::class, 'update']);
    Route::get('/post/delete/{id}', [PostController::class, 'delete']);
});

Route::get('/', function () {
    return view('welcome'); // resources/views/welcome.blade.php
});

Route::get('/contactus', function () {
    echo "Contact us";
});

Route::get('/aboutus', function () {
    echo "About Us";
});

// http://lara9tot.test/home
Route::get('/home', function () {
    $msj = 'Welcome to my portal';
    $data = ['msj' => $msj, 'number' => 1000];
    return view('home', $data); // resources/views/home.blade.php
});

// http://lara9tot.test/hello
Route::get('/hello', [HelloController::class, 'world']);

// http://lara9tot.test/user-list
Route::get('/user-list', [HelloController::class,'showAllUsers']);

// http://lara9tot.test/user/1
Route::get('/user/{id}', [HelloController::class,'showUser']);
