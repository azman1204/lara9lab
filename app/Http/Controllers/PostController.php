<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;

class PostController extends Controller {
    // show form
    function create() {
        $categories = Category::pluck('id', 'title');
        //dd($categories); ['PHP' => 1, 'Laravel' => 2, ...]
        //$data['categories'] = $categories;
        echo view('dashboard.post.create', compact('categories'));// dahsboard/post/create.blade.php
        //return view('dashboard/post/create');
    }

    function store(Request $request) {
        //dd($request->all()); // return semua data
        $request->validate([
            'title'       => 'required|min:5|max:500',
            'slug'        => 'required|min:5|max:500',
            'content'     => 'required|min:7',
            'category_id' => 'required|integer',
            'description' => 'required|min:7',
            'posted'      => 'required'
        ]);

        Post::create($request->all());
        return redirect('/post/list');
    }

    // lara9lab.test/post/list
    function list(Request $request) {
        // [1 => 'PHP', 2 => 'Laravel']
        $categories = Category::pluck('title', 'id');
        //$posts = Post::all();
        $title = $request->title;
        //$posts = Post::where('title', 'LIKE', "%$title%")->get();
        $query = Post::where('title', 'LIKE', "%$title%");

        if (! empty($request->category)) {
            $query = $query->where('category_id', '=', $request->category);
        }

        // $posts = $query->get();
        $posts = $query->paginate(2);
        return view('dashboard.post.list', compact('posts', 'categories'));
    }

    function edit($id) {
        $categories = Category::pluck('id', 'title');
        $post = Post::find($id);
        return view('dashboard.post.edit', compact('post', 'categories'));
    }

    function update(Request $request) {
        // validation
        $request->validate([
            'title'       => 'required|min:5|max:500',
            'slug'        => 'required|min:5|max:500',
            'content'     => 'required|min:7',
            'category_id' => 'required|integer',
            'description' => 'required|min:7',
            'posted'      => 'required'
        ]);
        $post = Post::find($request->id); // cari by pk
        $post->update($request->all());
        return redirect('/post/list');
    }

    function delete($id) {
        Post::find($id)->delete();
        return redirect('/post/list');
    }
}
