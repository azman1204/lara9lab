<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;

class LoginController extends Controller {
    // show login form
    function login() {
        return view('login');
    }

    // authentication
    function auth(Request $request) {
        //echo \Hash::make('1234'); exit;// hashing password
        $email = $request->email;
        $password = $request->password;
        $user = User::where('email', $email)->first();// first() - return an object or null (not found)
        if ($user) {
            //echo 'Email exist';
            if (\Hash::check($password, $user->password)) {
                // matched
                \Auth::login($user); // simpan maklumat user yg logged in
                session(['logged-in' => true]);
                return redirect('/post/list');
            } else {
                return redirect('/login')->with('msg', 'Login failed');
            }
        } else {
            return redirect('/login')->with('msg', 'Login failed');
        }
    }

    // logout
    function logout() {
        \Auth::logout();
        session()->flush(); // destroy all session data
        return redirect('/login');
    }
}
