<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\SysUser;

class HelloController extends Controller {
    function world() {
        return 'Hello World...';
    }

    // select * from users
    function showAllUsers() {
        $user = SysUser::all(); // return array of SysUser objects
        //dd($user); // dd = die dump
        $data['user'] = $user;
        return view('user_list', $data);
    }

    // display maklumat detail seorang user
    // select * from user where id = 1
    function showUser($id) {
        $user = SysUser::find($id);// return an obj
        //dd($user);
        $data['user'] = $user;
        return view('user_detail', $data);
    }
}
