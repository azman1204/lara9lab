<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;

class TestMiddleware {
    public function handle(Request $request, Closure $next) {
        if (session()->has('logged-in')) {
            // user has been logged in successfully
            return $next($request);
        } else {
            // user not logged in
            return redirect('/login');
        }
    }
}
