<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SysUser extends Model {
    public $table = 'users'; // model ini mewakili table users
    public $primaryKey = 'id';
}
