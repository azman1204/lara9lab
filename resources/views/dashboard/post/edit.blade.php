@extends('dashboard.layout')
@section('content')
@include('dashboard.fragment._errors-form')

<form action="/post/update" method="post">
    <input type="hidden" name="id" value="{{ $post->id }}">
    @csrf
    <label>Title</label>
    <input type="text" name="title" value="{{ old('title', $post->title) }}">

    <label>Slug</label>
    <input type="text" name="slug" value="{{ old('slug', $post->slug) }}">

    <label>Category</label>
    <select name="category_id">
        <option value="">-- Please Choose --</option>
        @foreach ($categories as $title => $id)
            <option value="{{ $id }}" @if (old('category_id', $post->category_id) == $id) selected @endif>
                {{ $title }}
            </option>
        @endforeach
    </select>

    <label>Post ?</label>
    <select name="posted">
        <option value="not" @if (old('posted', $post->posted) == 'not') selected @endif>No</option>
        <option value="yes" @if (old('posted', $post->posted) == 'yes') selected @endif>Yes</option>
    </select>

    <label>Content</label>
    <textarea name="content">{{ old('content', $post->content) }}</textarea>

    <label>Description</label>
    <textarea name="description">{{ old('description', $post->description) }}</textarea>

    <button type="submit">Submit</button>
</form>

@endsection
