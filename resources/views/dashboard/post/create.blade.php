@extends('dashboard.layout')
@section('content')
@include('dashboard.fragment._errors-form')

<form action="{{ route('post.store') }}" method="post" class="col-md-8">
    @csrf
    <div class="row mb-1">
        <div class="col-md-1">
            <label>Title</label>
        </div>
        <div class="col-md-11">
            <input type="text" name="title" value="{{ old('title') }}" class="form-control">
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-1">
            <label>Slug</label>
        </div>
        <div class="col-md-11">
            <input type="text" name="slug" value="{{ old('slug') }}" class="form-control">
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-1">
            <label>Category</label>
        </div>
        <div class="col-md-11">
            <select name="category_id" class="form-control">
                <option value="">-- Please Choose --</option>
                @foreach ($categories as $title => $id)
                    <option value="{{ $id }}" @if (old('category_id') == $id) selected @endif>{{ $title }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-1">
            <label>Post ?</label>
        </div>
        <div class="col-md-11">
            <select name="posted" class="form-control">
                <option value="not" @if (old('posted') == 'not') selected @endif>No</option>
                <option value="yes" @if (old('posted') == 'yes') selected @endif>Yes</option>
            </select>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-1">
            <label>Content</label>
        </div>
        <div class="col-md-11">
            <textarea name="content" class="form-control">{{ old('content') }}</textarea>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-1">
            <label>Description</label>
        </div>
        <div class="col-md-11">
            <textarea name="description" class="form-control">{{ old('description') }}</textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

@endsection()
