@extends('dashboard.layout')
@section('title', 'Posting List')
@section('content')
<!-- search form -->
<form action="/post/list" method="post">
    @csrf
    <div class="row">
        <div class="col-md-2">
            <input type="text" value="{{ request()->title }}" class="form-control" placeholder="title" name="title">
        </div>

        <div class="col-md-2">
            <select class="form-control" name="category">
                <option value="">-- Please Choose --</option>
                @foreach ($categories as $key => $value)
                    <option value="{{ $key }}" @if (request()->category == $key) selected @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-2">
            <input type="submit" value="Submit" class="btn btn-primary">
        </div>
    </div>
</form>

<a href="/post/create" class="btn btn-primary mb-1 mt-1">Create</a>

<table class="table table-striped table-bordered table-hover">
    <tr>
        <th>No</th>
        <th>Title</th>
        <th>Category</th>
        <th>Posted</th>
        <th>Action</th>
    </tr>
    @foreach ($posts as $post)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $categories[$post->category_id] }}</td>
            <td>{{ $post->posted }}</td>
            <td>
                <a href='/post/edit/{{ $post->id }}' class="btn btn-success btn-sm">Edit</a>
                <a href='/post/delete/{{ $post->id }}' class="btn btn-danger btn-sm">Delete</a>
            </td>
        </tr>
    @endforeach
</table>

{{ $posts->links() }}

<style>
    .active>.page-link, .page-link.active {
        background-color: crimson;
    }
</style>

@endsection()
