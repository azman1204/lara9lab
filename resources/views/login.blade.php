<!doctype html>
<html lang="en">
  <head>
    <title>Login</title>
    <link href="/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    @if(session()->has('msg'))
        <div class="alert alert-danger">{{ session('msg') }}</div>
    @endif

    <form action="/auth" method="post">
        @csrf
        <div class="row mb-1">
            <div class="col-md-12">
                <input type="text" name="email" placeholder="Email Address" class="form-control">
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-12">
                <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-12">
                <input type="submit" value="Submit" class="btn btn-primary w-100">
            </div>
        </div>
    </form>
  </body>

  <style>
    body {
        padding: 25%;
    }
  </style>
</html>
