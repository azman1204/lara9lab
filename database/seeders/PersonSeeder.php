<?php
namespace Database\Seeders;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Person;

class PersonSeeder extends Seeder {
    public function run() {
        for($i=1; $i <= 100; $i++) {
            $person = new Person();
            $person->name = "name $i";
            $person->save(); // insert into database
        }
    }
}
